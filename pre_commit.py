#!/usr/bin/env python3
import os
import stat
import argparse

CONTENT = """#!/bin/sh
#
# An example hook script to verify what is about to be committed.
# Called by "git commit" with no arguments.  The hook should
# exit with non-zero status after issuing an appropriate message if
# it wants to stop the commit.

exec 1>&2

if ! mypy .; then
    cat <<EOF
Error: mypy found an error!
EOF
    exit 1
fi

if ! prospector .; then
    cat <<EOF
Error: prospector found an error!
EOF
    exit 1
fi
"""


def main() -> None:
    parser = argparse.ArgumentParser(description='Install pre-commit hook to git hooks')
    parser.add_argument('--path', '-p', default='.', help='path to repository')
    args = parser.parse_args()

    file_path = os.path.join(args.path.rstrip('/'), '.git/hooks/pre-commit')

    with open(file_path, 'w') as fd:  # pylint: disable=invalid-name
        fd.write(CONTENT)

    state = os.stat(file_path)
    os.chmod(file_path, state.st_mode | stat.S_IEXEC)


if __name__ == '__main__':
    main()
